Unity Controls for Alfred
============

AppleScript Extension to control Unity from [Alfred App](http://alfredapp.com/). You will need Alfred and the [Powerpack](http://www.alfredapp.com/powerpack/) to use this.

Commands
----------------

    u play    ||  Plays the currently open project, aslo accepts 'p'
    u run     ||  Build & Run the current project, aslo accepts 'r', 'build', 'br', 'b'

Notes
----------------
Supports Extension Updater but it doesn't look like it supports applescript extensions.
Please let me know if you have any requests or find any issues.


Download
----------------
[Extension](https://github.com/downloads/ghelton/AlfredUnity/AlfredUnity.alfredextension)


## Version History ##

### 0.2 - December 29, 2011###

- Updated Readme
- Fixed run command and added a few extra aliases

### 0.1 - December 28, 2011###

- Initial Release